import settings from '../../common/settings'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
import * as uiv from 'uiv'
import rccAccount from './modules/rcc-account.vue'

const router = new VueRouter()

router.mode = 'html5'

export function testRccComponent() {
  Vue.use(VueI18n)
  Vue.use(VueRouter)
  Vue.use(uiv,
    {
      prefix: 'uiv'
    })
  let selectedLocale = settings.selectedLocale
  let messages = {}
  messages[selectedLocale] = window.translationObject
  let i18n = new VueI18n({
    locale: selectedLocale, // set locale
    messages
  })
  let app = new Vue({
    i18n,
    router,
    provide: {
      'provideSomeOption': 'value' //how to provide data to vue component
    },
    render: h => h(rccAccount)
  })
  app.$mount('rcc-account')
}