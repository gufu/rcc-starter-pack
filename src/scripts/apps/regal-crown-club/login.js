import settings from '../../common/settings'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
import * as uiv from 'uiv'
import loginForm from './modules/login-form.vue'

const router = new VueRouter()

router.mode = 'html5'

export function testLoginComponent() {
    Vue.use(VueI18n)
    Vue.use(VueRouter)
    Vue.use(uiv,
        {
            prefix: 'uiv'
        })
    let selectedLocale = settings.selectedLocale
    let messages = {}
    messages[selectedLocale] = window.translationObject
    let i18n = new VueI18n({
        locale: selectedLocale, // set locale
        messages
    })
    let app = new Vue({
        i18n,
        router,
        render: h => h(loginForm)
    })
    app.$mount('login')
}