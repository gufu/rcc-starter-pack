import cookies from 'js-cookie'
import logger from './logger'

const cookieOptions = {
    expires: 7,
    secure: window.location.protocol.indexOf('https:') !== -1
}

const keyList = 'userLocation,' +
    'geolocationEnabled,' +
    'geolocationLastReadTimestamp,' +
    'geolocationResultsLastUsedSource,' +
    'geolocationLastSelectedLocation,' +
    'selectedSessionCinemaCode,' +
    'selectedSessionCinemaObject,' +
    'testCookieForExamplePurposes'

const keys = keyList.split(',')

let validCookieName = function (name) {
    return typeof name !== 'undefined' && keys.indexOf(name) !== -1
}

let getCookie = function (name) {
    if (validCookieName(name)) {
        return cookies.get(name)
    } else {
        logger.error('Method cookies.getCookie() failed. Invalid param "name". ', name)
    }
}

let getJson = function (name) {
    if (validCookieName(name)) {
        return cookies.getJSON(name)
    } else {
        logger.error('Method cookies.getJson() failed. Invalid param "name". ', name)
    }
}

let setCookie = function (name, value) {
    if (validCookieName(name)) {
        logger.info('Cookie "' + name + '" set to: ', value)
        cookies.set(name, value, cookieOptions)
    } else {
        logger.error('Method cookies.setCookie() failed. Invalid param "name". ', name)
    }
}

let removeCookie = function (name) {
    if (validCookieName(name)) {
        logger.info('Cookie "' + name + '" removed.')
        cookies.remove(name, cookieOptions)
    } else {
        logger.error('Method cookies.removeCookie() failed. Invalid param "name". ', name)
    }
}

export default {
    get: getCookie,
    getJson: getJson,
    set: setCookie,
    remove: removeCookie
}