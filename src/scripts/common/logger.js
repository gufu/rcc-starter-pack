import logger from 'beaver-logger'

/*
### https://github.com/krakenjs/beaver-logger

Basic logging

$logger.info(<event>, <payload>);
$logger.warn(<event>, <payload>);
$logger.error(<event>, <payload>);
Queues a log. Options are debug, info, warn, error.

For example:

$logger.error('something_went_wrong', { error: err.toString() })

$logger.track(<payload>);
Call this to attach general tracking information to the current page. This is useful if the data is not associated with a specific event, and will be sent to the server the next time the logs are flushed.
*/
logger.init({
  autoLog: ['warn', 'error', 'info', 'debug'],
  heartbeat: false,
  logLevel: 'debug',
  logPerformance: false

})

export default {
  debug: logger.debug,
  info: logger.info,
  warn: logger.warn,
  error: logger.error
}
