/* global tenantId, selectedLocale, isRtl */

let fallbackTenantId = ''
let fallbackSelectedLocale = ''
let fallbackisRtl = false

let breakpoints = { // default breakpoints - customize this
  sm: 768,
  md: 980,
  lg: 1920,
  xl: Infinity
}

let cookieOptions = {
  maxAge: 60 * 60 * 24 * 30, // 30 days
  expires: 30, // 30 days
  secure: false,
}

if (typeof tenantId !== 'undefined') {
  fallbackTenantId = tenantId
}

if (typeof selectedLocale !== 'undefined') {
  fallbackSelectedLocale = selectedLocale
}

if (typeof isRtl !== 'undefined') {
  fallbackisRtl = isRtl
}

let getTenantId = function () {
  return fallbackTenantId || window.tenantId
}

let getSelectedLocale = function () {
  return fallbackSelectedLocale || window.selectedLocale
}

export default {
  tenantId: getTenantId(),
  selectedLocale: getSelectedLocale(),
  isRtl: fallbackisRtl || window.isRtl,
  breakpoints: breakpoints,
  cookieOptions: cookieOptions,
}
