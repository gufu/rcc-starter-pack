import './common/polyfills'
import { testLoginComponent } from './apps/regal-crown-club/login'
import { testRccComponent } from './apps/regal-crown-club/test-rcc-component'

(function () {
    testRccComponent()
    testLoginComponent()
})()