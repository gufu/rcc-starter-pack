# Regal Crown Club - starter pack
Hi!
This is a basic project setup for **Regal Crown Club** signup, registration, login and user account application(s). This project was made so that externally written application will be compatible with current frontend stack of upcoming **Regal website project**.


# Prerequisities

This is what you need to start this project:

 - nodejs (tested v8.11.2)
 - npm (tested v6.1.0)

Helpful and nice to have:

 - npm install -g http-server // good for running static html files from simple http server to test operations on cookies
 - some coding skills ;)


# Running the project

`npm install` - install all dependencies

`npm build` - build bundle file based on configuration file

`npm run dev` - runs "rollup -c --watch" with hot reload of compiled bundle files


# Project tech & structure
Main elements used in frontend part of the project:

 - Vue.js (https://vuejs.org/)

 - Rollup bundler (https://github.com/rollup/rollup)

 - Bootstrap 3 (https://getbootstrap.com/docs/3.3/)


# Project files:
###
**rollup.config.js**  - configuration for rollup bundler.
Example bundle configuration was added: `const rcc_page_bundle`
Each bundle configuration is run in `export default` section by calling `Object.assign({}, rcc_page_bundle, COMMON)` which basically takes common config for all bundles, aplies specific parameters for rcc_page_bundle and build final javascript file, exported to location defined in `{output: { file: '/path' } }`
###
**/src/scripts/test-view-page-bundle.js** - this is a bundle source when you include all vue components that will be bundled into a final javascript file. This allows us to write separate vue components and bundle them together per page as needed.
###
**/src/scripts/common/**  contains some of common libs already used in the project. Most importantly during development you have to use `cookies.js`, `logger.js`, `polyfills.js`, `settings.js`.
###
**/src/scripts/common/cookie.js** wrapper functions for `js-cookie` library, so that we have a list of all cookies used across frontend platform in one place. All additional cookies need to be added to `const keyList` as each `get/set/getJson/remove` method checks if passed cookie name isn't some random string. This ensures all cookie operations are made consciously and no garbage cookies are created as temporary solutions.
###
**/src/scripts/common/logger.js** wrapper for `krakenjs/beaver-logger` library (https://github.com/krakenjs/beaver-logger), simply to have one config for all logger instances across different components.
###
**/src/scripts/common/polyfills.js** here is where we add missing polyfills for older browsers as needed
###
**/src/scripts/common/settings.js** this lets us control some global aspects of the website that can't be easily imported from java backend. Normally some of these parameters are rendered in the context, or passed globally in window object. Most probably you won't have to touch this, except use `tenantId` when adding external images. Let's say we use tenant "10108", so path to any image file should contain this tenant, eg. '/src/images/10108/logo.png'. This way we can use the same component with different branding.
###
**/src/scripts/apps/regal-crown-club/login.js** example vuejs login component
###
**/src/scripts/apps/regal-crown-club/test-rcc-component.js** example vuejs RCC component
###
**/dist/stylesheets/*** stylesheets compiled for tenant '10108' as an example. If any other stylsheets are required, please bundle them inside components if they'are very specific to an element of the component, or create another css file if some of the classes will be reusable across different components. We encourage to add scss source for your stylesheets
###
**/dist/javascript/** this is were example bundle is being built
###
**/dist/index.html** example html view for compiled components. It contains some important elements.
- `window.translationObject` - existing translations passed to vuejs components
- `let additionalTranslations` - object for defining additional translations appended to global object
- `window.tenantId = 10108;`- global config, no need to change that
- `window.selectedLocale = 'en_US';`- global config, no need to change that
- `window.isRtl = false;`- global config, no need to change that

## Additional help required?
Contact us:

t.stulka@cinema-city.pl - Tomasz Stułka - Lead Frontend Developer

k.gusz@cinema-city.pl - Konrad Gusz - Senior Fronted Developer

