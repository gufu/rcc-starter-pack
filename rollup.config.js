import { uglify } from 'rollup-plugin-uglify'
import babel from 'rollup-plugin-babel'
import eslint from 'rollup-plugin-eslint'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import replace from 'rollup-plugin-replace'
import json from 'rollup-plugin-json'
import postcss from 'rollup-plugin-postcss'
import vue from 'rollup-plugin-vue2'
import builtins from 'rollup-plugin-node-builtins'
import globals from 'rollup-plugin-node-globals'
import svg from 'rollup-plugin-vue-inline-svg'

// PostCSS plugins

import simplevars from 'postcss-simple-vars'
import nested from 'postcss-nested'
import cssnext from 'postcss-cssnext'
import cssnano from 'cssnano'
// import scss from 'rollup-plugin-scss'
// Rollup plugins
const outputGlobals = {
  jquery: '$',
  Vue: 'vue',
  window: 'window'
}

// single bundle generated from 'input' to 'file' location
const rcc_page_bundle = {
  input: 'src/scripts/test-view-page-bundle.js',
  output: {
    format: 'iife',
    sourcemap: true,
    file: 'dist/javascript/test-view-page-bundle.min.js',
    name: 'rcc_page_bundle',
    globals: outputGlobals
  }
}

// please don't modify const COMMON
const COMMON = {
  treeshake: true,
  plugins: [
    uglify(),
    svg({
      removeDoctype: true,
      removeXMLProcInst: true,
      removeComments: true,
      removeMetadata: true,
      removeDesc: true,
      removeUselessDefs: true,
      removeEditorsNSData: true,
      removeEmptyText: true,
      convertStyleToAttrs: true,
      convertColors: true
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
      exclude: 'node_modules/**'
    }),
    json({
      preferConst: true, // Default: false
    }),
    vue({autoStyles: false, styleToImports: true, compileTemplate: true}),
    postcss({
      plugins: [
        simplevars(),
        nested(),
        cssnext({warnForDuplicates: false}),
        cssnano(),
      ],
      extensions: ['.css'],
    }),
    resolve({
      jsnext: true,
      main: true,
      browser: true,
      extensions: ['.js', '.json', '.vue']
    }),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/vue/dist/**': ['vue']
      }
    }),
    eslint({
      exclude: [
        'src/styles/**',
        'src/scripts/translations/**',
        'src/scripts/icons/**',
        'node_modules/**'
      ]
    }),
    babel({
      exclude: ['node_modules/**']
    }),
    builtins(),
    babel(),
    globals()
  ],
  external: ['$', 'jquery']
}

export default [
  Object.assign({}, rcc_page_bundle, COMMON),
  // Object.assign({}, non_existing_bundle, COMMON)

]
